package week1;/*Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

        Example:

        Input: [0,1,0,3,12]
        Output: [1,3,12,0,0]
        Note:

        You must do this in-place without making a copy of the array.
        Minimize the total number of operations.*/

import java.util.Arrays;

public class MoveZeroes {
    public static void main(String[] args) {
        int[] arr={0,1,4,2,0,6,0,0,2,3,4,0};
        int[] arr1={1,3,2,0,2,4,2,3,0,2,4};
        int[] arr2={1,2,1,2,1,3,1,23,0};
        int[] arr3={0,0,0,0,0,1,0,0,0,0};
        MoveZeroes zeroes = new MoveZeroes();
        zeroes.moveZeroes1(arr);
        zeroes.moveZeroes1(arr1);
        zeroes.moveZeroes1(arr2);
        zeroes.moveZeroes1(arr3);
        /*zeroes.moveZeroes(arr);
        zeroes.moveZeroes(arr1);
        zeroes.moveZeroes(arr2);
        zeroes.moveZeroes(arr3);*/



    }
    public void moveZeroes(int[] nums) {
        System.out.println(Arrays.toString(nums));
        int q = 0;
        for (int i = 0; i < (nums.length - q); i++
        ) {
            if (nums[i] == 0) {
                for (int j = i; j < (nums.length - 1); j++
                ) {
                    nums[j] = nums[j + 1];
                }
                nums[nums.length - 1] = 0;
                i--;
                q++;
            }
            System.out.println(Arrays.toString(nums));
        }
    }

        //better optimised
    public void moveZeroes1(int[] nums) {
            int i=0;
            int j=0;
            while (j<nums.length){
                if (nums[j]!=0) {
                    nums[i++] = nums[j];
                }
                    j++;
            }
            while (i<nums.length){
                nums[i++]=0;
            }
            System.out.println(Arrays.toString(nums));
    }
}
