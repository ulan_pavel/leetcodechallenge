package week1;/*Given an array of strings, group anagrams together.

        Example:

        Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
        Output:
        [
        ["ate","eat","tea"],
        ["nat","tan"],
        ["bat"]
        ]
        Note:

        All inputs will be in lowercase.
        The order of your output does not matter.*/

import java.util.*;


class GroupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        HashMap<String, List<String>> map = new HashMap();
        List<List<String>> result = new ArrayList();
        for(String s : strs){
            String sortS = sortString(s);
            List<String> list = new ArrayList();
            if(map.containsKey(sortS)){
                list = map.get(sortS);
            }
            list.add(s);
            map.put(sortS, list);
        }
        for(String key : map.keySet()){
            result.add(map.get(key));
        }
        return result;
    }

    private String sortString(String s){
        char[] array = s.toCharArray();
        Arrays.sort(array);
        return String.valueOf(array);
    }

    public static void main(String[] args) {
        String[] array = {"eat", "tea", "tan"," ", "ate", "nat", "bat","","cat","cat"," ",""};
        GroupAnagrams solution = new GroupAnagrams();

        System.out.println(solution.groupAnagrams(array));
    }
}


