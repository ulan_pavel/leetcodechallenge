package week1;/*Given an integer array arr, count element x such that x + 1 is also in arr.

        If there're duplicates in arr, count them seperately.



        Example 1:

        Input: arr = [1,2,3]
        Output: 2
        Explanation: 1 and 2 are counted cause 2 and 3 are in arr.
        Example 2:

        Input: arr = [1,1,3,3,5,5,7,7]
        Output: 0
        Explanation: No numbers are counted, cause there's no 2, 4, 6, or 8 in arr.
        Example 3:

        Input: arr = [1,3,2,3,5,0]
        Output: 3
        Explanation: 0, 1 and 2 are counted cause 1, 2 and 3 are in arr.
        Example 4:

        Input: arr = [1,1,2,2]
        Output: 2
        Explanation: Two 1s are counted cause 2 is in arr.


        Constraints:

        1 <= arr.length <= 1000
        0 <= arr[i] <= 1000*/

import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

public class CountingElements {
    public static void main(String[] args) {
        CountingElements elements = new CountingElements();
        int[] arr={1,3,2,3,5,0};
        int[] arr1={1,1,3,3,5,5,7,7};
        int[] arr2={1,3,2,3,2,0};
        int[] arr3={1,3,2};
        elements.countElements1(arr);
        elements.countElements1(arr1);
        elements.countElements1(arr2);
        elements.countElements1(arr3);



    }
    public int countElements(int[] arr) {
        HashSet<Integer> integers = new HashSet<Integer>();
        for (int i = 0; i<arr.length; i++){
            for (int j=0;j<arr.length; j++){
                if (arr[j]==((arr[i])+1)){
                 integers.add(arr[i]);
                }
            }
        }
        int count=0;
        for (int i=0;i<arr.length;i++){
            if (integers.contains(arr[i])) {
                count++;
            }
        }
        return count;
    }
    public int countElements1(int[] arr) {
        HashSet<Integer> integers = Arrays.stream(arr).boxed().collect(Collectors.toCollection(HashSet::new));
        int count=0;
        for (int i = 0; i<arr.length; i++){
            if (integers.contains((arr[i])+1)) {
                count++;
            }
        }
        return count;
    }
}
