package week1;/*
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

        Example:

        Input: [-2,1,-3,4,-1,2,1,-5,4],
        Output: 6
        Explanation: [4,-1,2,1] has the largest sum = 6.
        Follow up:

        If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
*/

public class MaximumSubarray {
    public static void main(String[] args) {
        MaximumSubarray subarray = new MaximumSubarray();
        int[] nums={-28,3,1,4,5,-2,6,-15,8,-8,2};
        System.out.println(subarray.maxSubArray(nums));
    }
    public int maxSubArray(int[] arr) {
        int maxEndHere = arr[0];
        int maxSoFar = arr[0];
        for(int i=1;i<arr.length;i++){
            maxEndHere = Math.max(arr[i], maxEndHere+arr[i]);
            maxSoFar = Math.max(maxSoFar,maxEndHere);
        }
        return maxSoFar;
    }

}
