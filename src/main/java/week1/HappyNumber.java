package week1;/*Write an algorithm to determine if a number n is "happy".

        A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.

        Return True if n is a happy number, and False if not.*/

class HappyNumber {
    public static void main(String[] args) {
        HappyNumber happyNumberTest = new HappyNumber();
        int n = 7;
        System.out.println(happyNumberTest.isHappy(n));
    }

    public boolean isHappy(int n) {

        if (n < 0) return false;
        int s = n;
        int d = 0;
        while (s > 9) {
            n = s;
            s = 0;
            while (n > 0) {
                d = n % 10;
                s = s + (d * d);
                n = n / 10;
            }
        }
        if (s == 7) return true;
        return s == 1;
    }
}
