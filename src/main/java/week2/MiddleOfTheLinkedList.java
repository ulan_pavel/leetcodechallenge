/**Given a non-empty, singly linked list with head node head, return a middle node of linked list.

        If there are two middle nodes, return the second middle node.

        Example 1:

        Input: [1,2,3,4,5]
        Output: Node 3 from this list (Serialization: [3,4,5])
        The returned node has value 3.  (The judge's serialization of this node is [3,4,5]).
        Note that we returned a ListNode object ans, such that:
        ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next = NULL.
        Example 2:

        Input: [1,2,3,4,5,6]
        Output: Node 4 from this list (Serialization: [4,5,6])
        Since the list has two middle nodes with values 3 and 4, we return the second one.


        Note:

        The number of nodes in the given list will be between 1 and 100.**/
package week2;

import com.sun.org.apache.xpath.internal.res.XPATHErrorResources_sv;

import java.util.Arrays;
import java.util.LinkedList;

public class MiddleOfTheLinkedList {
    public static void main(String[] args) {
        int a=6, b =5;
        System.out.println(a%2+" "+a/2 +"  "+ b%2+" "+b/2);
        ListNode listNode= new ListNode();
        listNode.add(1);
        listNode.add(2);
        listNode.add(3);
        listNode.add(4);
        listNode.add(5);
        listNode.add(6);
        MiddleOfTheLinkedList middle = new MiddleOfTheLinkedList();
        System.out.println(middle.middleNode1(listNode));
        System.out.println(middle.middleNode(listNode));

    }
    /**
     * Definition for singly-linked list.
     * public class ListNode {
     *     int val;
     *     ListNode next;
     *     ListNode(int x) { val = x; }
     * }
     */
        public ListNode middleNode(ListNode head) {
                int lenth=0;
                ListNode node = head;
                while (head.next!=null){
                    head=head.next;
                    lenth++;
                }
                for(int i=0; i<(lenth/2);i++){
                    node=node.next;
                }
                if (lenth%2!=0){
                    node=node.next;
                }
                return node;
        }
        //Best sample
        public ListNode middleNode1(ListNode head) {
            ListNode slow = head;
            ListNode fast = head;
            while (fast != null && fast.next !=null) {
                slow = slow.next;
                fast = fast.next.next;
            }

            return slow;
        }
}
class ListNode extends LinkedList {
          int val;
          ListNode next;
          ListNode(int x) { val = x; }

    public ListNode() {

    }
}
